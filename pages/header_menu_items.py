import re
from pages.base_page import BasePage

class HeaderMenuItems(BasePage):
    PSB_LOGO_ID = BasePage.APP_PACKAGE + ':id/logo'
    USER_BALANCE_ID = BasePage.APP_PACKAGE + ':id/wallet'
    BACK_BUTTON_ID = BasePage.APP_PACKAGE + ':id/mainAction'



    def get_user_balance(self):
        user_balance = self.driver.find_element_by_id(self.USER_BALANCE_ID)
        return re.search("r'£(.+)'", user_balance.text)


