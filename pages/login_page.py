from pages.base_page import BasePage
from helpers_methods.taps_helpers import TapsHelpers

class LoginPage(BasePage):
    EMAIL_FIELD_ID = BasePage.APP_PACKAGE + ':id/email'
    PASSWORD_FIELD_ID = BasePage.APP_PACKAGE + ':id/password'
    SIGN_IN_BUTTON_ID = BasePage.APP_PACKAGE + ':id/signInButton'
    PERMISSION_DENIED_MSG_ID = BasePage.APP_PACKAGE + ':id/textinput_error'
    SIGN_UP_LABEL_ID = BasePage.APP_PACKAGE + ':id/signUpLabel'



    def type_email(self, email):
        email_field = self.driver.find_element_by_id(self.EMAIL_FIELD_ID)
        email_field.send_keys(email)

    def type_password(self, password):
        password_field = self.driver.find_element_by_id(self.PASSWORD_FIELD_ID)
        password_field.send_keys(password)


    def tap_sign_in_button(self):
        sign_in_button = self.driver.find_element_by_id(self.SIGN_IN_BUTTON_ID)
        sign_in_button.click()

    def click_join_the_game(self):
        dimension = self.driver.get_window_size()
        print(dimension)
        tap_to_link = TapsHelpers(self.driver)
        tap_to_link.tap_spannable_element(self.SIGN_UP_LABEL_ID, 0.75)

    def is_invalid_login_message_displayed(self):
        permission_denied_msg = self.driver.find_element_by_id(self.PERMISSION_DENIED_MSG_ID)
        return permission_denied_msg.is_displayed()

    def log_in(self, email, password):
        print("Using email: " + email + ", password: " + password)
        self.type_email(email)
        self.type_password(password)
        self.tap_sign_in_button()

