from appium.webdriver.common.touch_action import TouchAction
from selenium.common.exceptions import NoSuchElementException


class BasePage:
    def __init__(self, driver):
        """ Base constructor.

            Sets driver, implicit wait, and timeout.
        """
        self.driver = driver

    APP_PACKAGE = 'com.motorsport.psb.test.debug'

#    def tap_spannable_element(self, locator, offset_number):
#        element = self.driver.find_element_by_id(locator)
#
#        """getting element location coordinates"""
#        point = element.location
#
#        """calculating offset to the hyperlink text"""
#        offset_size = element.size['width'] * offset_number
#        x_offset = point['x'] + offset_size
#        y_offset = point['y'] + (element.size['height'] / 2)
#
#        """performing tap on the hyperlink text"""
#        action = TouchAction(self.driver)
#        action.tap(element=None, x=x_offset, y=y_offset).perform()
#
#    def swipe_to_down(self, start_percentage, final_percentage, anchor_percentage):
#        size = self.driver.get_window_size()
#        print(size)
#        anchor = size['width'] * anchor_percentage
#        start_point = size['height'] * start_percentage
#        end_point = size['height'] * final_percentage
#        action = TouchAction(self.driver)
#        action.press(anchor, start_point).move_to(anchor, end_point).release().perform()
#
#    def vertical_swipe_to_element(self, element):
#        found = False
#        while found == False:
#            try:
#                item1 = self.driver.find_element_by_xpath(element)
#                found = True
#                item1.click()
#            except NoSuchElementException:
#                size = self.driver.get_window_size()
#                print(size)
#                start_y = int(size['height'] * 0.5)
#                end_y = int(size['height'] * 0.2)
#                start_x = size['width'] / 2
#                action = TouchAction(self.driver)
#                action.press(x=start_x, y=start_y).move_to(x=start_x, y=end_y).release().perform()
#
#    def scroll_action_to_element(self, element, locator, click, start_point, end_point):
#
#        """ Выполнение скролинга с целью поиска эелемента который невидим на экране.
#                element - в этот аргумент передаем id или xpath локатор элемента, который надо найти
#                click - в этот аргумент передается значение True или False, в зависимости от того,
#                        нужно ли выполнить нажатие на элемент
#                success - показывает, найден ли элемент на экране в процессе цикла,
#                        если нет - выполняется новая итерация, success получает пустое значение
#                locator - в этот аргумент передается тип локатора: id, xpath, и тд
#        """
#        global item
#        success = False
#        while success is False:
#            try:
#                success = ""
#                self.element = element
#                if locator == "xpath":
#                    item = self.driver.find_element_by_xpath(element)
#                elif locator == "id":
#                    item = self.driver.find_element_by_id(element)
#                else:
#                    print("Locator not valid")
#
#                if click is True:
#                    item.click()
#            except NoSuchElementException:
#                dimension = self.driver.manage().window().size
#                print(dimension)
#                #                isEnd = "end"
#                #                if isEnd is True:
#                #                    return "notfound"
#                touch = TouchAction(self.driver)
#                touch.press(start_point).move_to(end_point).release().perform()
#                success = False
#        return "found"
