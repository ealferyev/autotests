from pages.base_page import BasePage


class DepositPage(BasePage):

    NEW_CARD_BUTTON_XPATH = "//android.widget.TextView[@text='New card']"
    SCREEN_TITLE_ID = BasePage.APP_PACKAGE + ":id/screenTitle"

    def click_to_add_new_card(self):
        new_card_button = self.driver.find_element_by_xpath(self.NEW_CARD_BUTTON_XPATH)
        new_card_button.click()

    