import random

from selenium.webdriver.common.by import By

from helpers_methods.swipe_helpers import SwipeHelpers
from pages.base_page import BasePage
from pages.header_menu_items import HeaderMenuItems


class MainPage(BasePage):

    SEARCH_BUTTON_ID = BasePage.APP_PACKAGE + ':id/mainAction'

    SERIES_LIST_ID = BasePage.APP_PACKAGE + ':id/tabLayout'
    SERIES_TAB = "android.support.v7.app.ActionBar$Tab"

    EVENTS_LIST_ID = BasePage.APP_PACKAGE + ':id/eventsList'
    EVENTS_NAME_ID = BasePage.APP_PACKAGE + ':id/eventName'

    MY_BOOK_ID = BasePage.APP_PACKAGE + ':id/myBook'
    FAVS_TAB_ID = BasePage.APP_PACKAGE + ':id/favorites'


    def set_series_name(self):
        """This method selects and sets the name of racing series """

        series_names = ["F1", "MotoGP", "NASCAR"]
        name = random.choice(series_names)
        return name

    def get_events_name(self):
        """This method will get the event name and prints it"""
        event_name = self.driver.find_element_by_id(self.EVENTS_NAME_ID).text
        return event_name



    def method(self):

        header = HeaderMenuItems(self.driver)
        print(header.get_user_balance())

        #events_name_container = set()
        #flag = True
#
#        scroll = SwipeHelpers(self.driver)
#        scroll.swipe_action(swipe_type="horizontal",
#                            swipe_direction="right",
#                            scrollable_element=self.EVENTS_LIST_ID)
        #while flag:
#
        #    events_names = self.driver.find_elements_by_id(self.EVENTS_NAME_ID)
        #    no_elements_in_container = True
        #    for element in events_names:
        #        if element.text in events_name_container:
        #            no_elements_in_container = False
        #            events_name_container.add(element.text)
        #    if no_elements_in_container:
        #        flag = True
        #    scroll.swipe_action(swipe_type="horizontal",
        #                        swipe_direction="right",
        #                        scrollable_element=self.EVENTS_LIST_ID)
        #print(events_name_container)


    def choose_event(self):
        # events_list = self.driver.find_elements(By.ID, self.EVENTS_LIST_ID)
        events_list = self.driver.find_elements(By.ID, self.EVENTS_LIST_ID)
        self.method()
        event = random.choice(events_list)
        event.click()

    def select_racing_series(self):
        """This method will select the racing series using a horizontal scroll """
        horizontal_scroll = SwipeHelpers(self.driver)
        horizontal_scroll.horizontal_scroll_and_click_element(scrollable_element="{0}".format(self.SERIES_LIST_ID),
                                                              found_element="{0}".format(self.set_series_name()))

    def select_racing_event(self):
        """This method will select the racing event using a horizontal scroll """
        horizontal_scroll = SwipeHelpers(self.driver)
        horizontal_scroll.horizontal_scroll_and_click_element(scrollable_element="{0}".format(self.EVENTS_LIST_ID),
                                                              found_element="{0}".format(self.get_events_name()))

    def open_my_book(self):
        """This method will open the 'My book' screen """
        my_book = self.driver.find_element_by_id(self.MY_BOOK_ID)
        my_book.click()
