from pages.base_page import BasePage


class CardDetailsPage(BasePage):
    SCREEN_TITLE_ID = BasePage.APP_PACKAGE + ":id/screenTitle"
    CARD_NUMBER_FIELD_ID = BasePage.APP_PACKAGE + ":id/cardNumber"
    EXPIRY_DATE_FIELD_ID = BasePage.APP_PACKAGE + ":id/expirationDate"
    CVV_CODE_FIELD_ID = BasePage.APP_PACKAGE + ":id/securityCode"
    CARD_HOLDER_FIELD_ID = BasePage.APP_PACKAGE + ":id/cardholder"
    FIRST_NAME_FIELD_ID = BasePage.APP_PACKAGE + ":id/firstName"
    LAST_NAME_FIELD_ID = BasePage.APP_PACKAGE + ":id/lastName"
    CONTINUE_BUTTON_ID = BasePage.APP_PACKAGE + ":id/continueBtn"

    def type_card_number(self, card_number):
        self.driver.find_element_by_id(self.CARD_NUMBER_FIELD_ID).send_keys(card_number)

    def type_expiry_date(self, expiry_date):
        self.driver.find_element_by_id(self.EXPIRY_DATE_FIELD_ID).send_keys(expiry_date)

    def type_cvv_code(self, cvv_code):
        self.driver.find_element_by_id(self.CVV_CODE_FIELD_ID).send_keys(cvv_code)

    def type_card_holder_name(self, card_holder_name):
        self.driver.find_element_by_id(self.CARD_HOLDER_FIELD_ID).send_keys(card_holder_name)

    def click_first_name_field(self):
        self.driver.find_element_by_id(self.FIRST_NAME_FIELD_ID).click()

    def type_first_name(self, first_name):
        self.driver.find_element_by_id(self.FIRST_NAME_FIELD_ID).send_keys(first_name)

    def type_last_name(self, last_name):
        self.driver.find_element_by_id(self.LAST_NAME_FIELD_ID).send_keys(last_name)

    def click_continue_button(self):
        continue_button = self.driver.find_element_by_id(self.CONTINUE_BUTTON_ID)
        continue_button.click()

    def type_users_card_details(self, card_number, expiry_date, cvv_code, card_holder_name):

            self.type_card_number(card_number)
            self.type_expiry_date(expiry_date)
            self.type_cvv_code(cvv_code)
            self.type_card_holder_name(card_holder_name)
            self.click_first_name_field()

            self.click_continue_button()
