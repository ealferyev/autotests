from pages.base_page import BasePage
from helpers_methods.swipe_helpers import SwipeHelpers

class OnboardingPage(BasePage):
    SKIP_BUTTON_ID = BasePage.APP_PACKAGE + ':id/signIn'
    PAGER_ID = BasePage.APP_PACKAGE + ':id/pager'
    TITLE_TV_ID = BasePage.APP_PACKAGE + ':id/titleTv'
    SIGN_UP_BUTTON = BasePage.APP_PACKAGE + ':id/signUpButton'

    def get_title(self):
        title = self.driver.find_element_by_id(self.TITLE_TV_ID)
        return title.text()

    def click_sign_up_button(self):
        right_swipe = SwipeHelpers(self.driver)
        right_swipe.swipe_to_element(swipe_type="horizontal", swipe_direction="right", element=self.SIGN_UP_BUTTON, scrollable_element=self.PAGER_ID, locator_type="id")

