from pages.base_page import BasePage
from pages.country_list import CountryListPage


class BillingAddressPage(BasePage):
    SCREEN_TITLE_ID = BasePage.APP_PACKAGE + ":id/screenTitle"
    COUNTRY_BUTTON_ID = BasePage.APP_PACKAGE + ":id/country"
    ADDRESS_FIELD_ID = BasePage.APP_PACKAGE + ":id/address"
    CITY_FIELD_ID = BasePage.APP_PACKAGE + ":id/city"
    POSTCODE_FIELD_ID = BasePage.APP_PACKAGE + ":id/zipCode"
    CONTINUE_BUTTON_ID = BasePage.APP_PACKAGE + ":id/continueBtn"

    def __init__(self, driver):
        super().__init__(driver)
        self.country_list = CountryListPage(self.driver)

    def open_country_list(self):
        self.driver.find_element_by_id(self.COUNTRY_BUTTON_ID).click()

    def type_address(self, address):
        self.driver.find_element_by_id(self.ADDRESS_FIELD_ID).send_keys(address)

    def type_city(self, city):
        self.driver.find_element_by_id(self.CITY_FIELD_ID).send_keys(city)

    def type_postcode(self, zip_code):
        self.driver.find_element_by_id(self.POSTCODE_FIELD_ID).send_keys(zip_code)

    def click_continue_button(self):
        continue_button = self.driver.find_element_by_id(self.CONTINUE_BUTTON_ID)
        continue_button.click()

    def type_billing_address(self, city, address, zip_code):

        self.open_country_list()
        self.country_list.select_uk_country()
        self.type_city(city)
        self.type_address(address)
        self.type_postcode(zip_code)
        self.click_continue_button()
