from pages.base_page import BasePage

class BalancePage(BasePage):

    DEPOSIT_BUTTON_ID = BasePage.APP_PACKAGE + ":id/deposit"
    WITHDRAW_BUTTON_ID = BasePage.APP_PACKAGE + ":id/withdraw"

    def click_deposit_button(self):
        deposit_button = self.driver.find_element_by_id(self.DEPOSIT_BUTTON_ID)
        deposit_button.click()

    def click_withdraw_button(self):
        withdraw_button = self.driver.find_element_by_id(self.WITHDRAW_BUTTON_ID)
        withdraw_button.click()

