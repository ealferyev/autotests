from pages.base_page import BasePage


class CardDetailsPage(BasePage):
    PAYMENT_AMOUNT_ID = BasePage.APP_PACKAGE + ":id/paymentAmount"
    CARD_INFO_ID = BasePage.APP_PACKAGE + ":id/creditCardInfo"
    LAST_NUMBERS_ID = BasePage.APP_PACKAGE + ":id/creditCardLastDigits"
    CONFIRM_BUTTON = BasePage.APP_PACKAGE + ":id/confirm"

    def get_amount_text(self):
        amount = self.driver.find_element_by_id(self.PAYMENT_AMOUNT_ID)
        return amount

    def click_confirm_button(self):
        confitm_button = self.driver.find_element_by_id(self.CONFIRM_BUTTON)
        confitm_button.click()
