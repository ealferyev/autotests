from pages.base_page import BasePage
from pages.login_page import LoginPage


class PinCodePage(BasePage):
    TITLE_ID = BasePage.APP_PACKAGE + ':id/pinCodeTitle'
    NUMBER_BUTTONS_ID = {
        "num0": BasePage.APP_PACKAGE + ':id/zero',
        "num1": BasePage.APP_PACKAGE + ':id/one',
        "num2": BasePage.APP_PACKAGE + ':id/two',
        "num3": BasePage.APP_PACKAGE + ':id/three',
        "num4": BasePage.APP_PACKAGE + ':id/four',
        "num5": BasePage.APP_PACKAGE + ':id/five',
        "num6": BasePage.APP_PACKAGE + ':id/six',
        "num7": BasePage.APP_PACKAGE + ':id/seven',
        "num8": BasePage.APP_PACKAGE + ':id/eight',
        "num9": BasePage.APP_PACKAGE + ':id/nine',
    }

    BACKSPACE_BUTTON_ID = BasePage.APP_PACKAGE + ':id/backspace'
    LOGOUT_BUTTON_ID = BasePage.APP_PACKAGE + ':id/logout'
    POINTS_ID = [
        BasePage.APP_PACKAGE + ':id/point1',
        BasePage.APP_PACKAGE + ':id/point2',
        BasePage.APP_PACKAGE + ':id/point3',
        BasePage.APP_PACKAGE + ':id/point4'
    ]

    def get_button(self, number_pad, key):
        for dict_key, dict_value in number_pad.items():
            if dict_key == key:
                return dict_value

    def enter_pin_code(self, *keys):

        for key in keys:
            button_id = self.get_button(self.NUMBER_BUTTONS_ID, key)

            numpad = self.driver.find_element_by_id(button_id)
            numpad.click()


    def enter_confirm_pin_code(self, *keys):
        for key in keys:
            v = self.get_button(self.NUMBER_BUTTONS_ID, key)

            numpad = self.driver.find_element_by_id(v)
            numpad.click()

    def return_to_login_page(self):

        logout_button = self.driver.find_element_by_id(self.LOGOUT_BUTTON_ID)
        logout_button.click()
        return LoginPage(self.driver)

    def title_is_displays(self):

        pin_code_screen_title = self.driver.find_element_by_id(self.TITLE_ID)
        return pin_code_screen_title.is_displayed()
