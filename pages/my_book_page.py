from pages.base_page import BasePage


class MyBookPage(BasePage):

    MY_BOOK_TAB_ID = BasePage.APP_PACKAGE + ':id/myBook'
    BALANCE_TAB_ID = BasePage.APP_PACKAGE + ':id/balance'
    P2P_TAB_ID = BasePage.APP_PACKAGE + ':id/p2p'
    SETTINGS_BUTTON_ID = BasePage.APP_PACKAGE + ':id/settings'
    INFO_BUTTON_ID = BasePage.APP_PACKAGE + ':id/rules'

    def open_settings(self):
        setting_button = self.driver.find_element_by_id(self.SETTINGS_BUTTON_ID)
        setting_button.click()

    def open_balance_screen(self):
        balance_tab = self.driver.find_element_by_id(self.BALANCE_TAB_ID)
        balance_tab.click()