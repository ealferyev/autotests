from pages.base_page import BasePage


class BaseAlert:
    def __init__(self, driver):
        self.driver = driver

    TITLE_ID = BasePage.APP_PACKAGE + ':id/md_title'
    CONTENT_TEXT_ID = BasePage.APP_PACKAGE + ':id/md_content'
    CONFIRM_BTN_ID = BasePage.APP_PACKAGE + ':id/md_buttonDefaultPositive'
    CANCEL_BTN_ID = BasePage.APP_PACKAGE + ':id/md_buttonDefaultNegative'

    def get_element_text(self, locator):
        element = self.driver.find_element_by_id(locator)
        return element.text

    def click_element(self, locator):
        element = self.driver.find_element_by_id(locator)
        element.click()


class ResendEmailAlert(BaseAlert):

    def resend_msg_is_displays(self):
        resend_msg = self.driver.find_element_by_id(self.CONTENT_TEXT_ID)
        return resend_msg.is_displayed()


class ConfirmLogoutAlert(BaseAlert):

    def click_confirm_logout(self):
        self.click_element(self.CONFIRM_BTN_ID)

    def click_cancel_logout(self):
        self.click_element(self.CANCEL_BTN_ID)
