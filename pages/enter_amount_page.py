from pages.base_page import BasePage


class AmountPage(BasePage):
    SCREEN_TITLE_ID = BasePage.APP_PACKAGE + ":id/screenTitle"
    AMOUNT_FIELD_ID = BasePage.APP_PACKAGE + ":id/amount"
    CONTINUE_BUTTON_ID = BasePage.APP_PACKAGE + ":id/continueBtn"

    def type_amount(self, amount):
        self.driver.find_element_by_id(self.AMOUNT_FIELD_ID).send_keys(amount)

    def click_continue_button(self):
        continue_button = self.driver.find_element_by_id(self.CONTINUE_BUTTON_ID)
        continue_button.click()
