from pages.base_page import BasePage


class CountryListPage(BasePage):

    COUNTRY_LIST_ID = BasePage.APP_PACKAGE + "id/countries"
    COUNTRY_NAME_ID = BasePage.APP_PACKAGE + ":id/countryName"
    COUNTRY_CALLING_CODE_ID = BasePage.APP_PACKAGE + ":id/countryCallingCode"


    def select_uk_country(self):
        uk_country = self.driver.find_element_by_xpath("//android.widget.TextView[@text='United Kingdom']")
        uk_country.click()

