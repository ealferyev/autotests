from pages.alerts import ConfirmLogoutAlert
from pages.base_page import BasePage
from helpers_methods.swipe_helpers import SwipeHelpers


class SettingsPage(BasePage):
    LOGOUT_BUTTON_XPATH = "//android.widget.TextView[@text='Log Out']"
    SOCIAL_TITLE_XPATH = "//android.widget.TextView[@text='Social networks']"
    CONFIRM_LOGOUT_ID = BasePage.APP_PACKAGE + ':id/md_buttonDefaultPositive'
    GAMBLING_LOGO_ID = BasePage.APP_PACKAGE + ':id/gamblingCommissionLogo'
    SETTING_LIST_ID = BasePage.APP_PACKAGE + ':id/contentContainer'
    LOGOUT_BUTTON_TEXT = "Log Out"

    def perform_users_logout(self):
        vertical_swipe = SwipeHelpers(self.driver)
        vertical_swipe.vertical_scroll_and_click_element(scrollable_element="{0}".format(self.SETTING_LIST_ID),
                                                         found_element="{0}".format(self.LOGOUT_BUTTON_TEXT))
        confirm_logout = ConfirmLogoutAlert(self.driver)
        confirm_logout.click_confirm_logout()

#        if search == "found":
#            confirm_logout = ConfirmLogoutAlert(self.driver)
#            confirm_logout.click_confirm_logout()
#        else:
#            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
