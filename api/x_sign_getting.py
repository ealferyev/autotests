import base64

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_PSS



url = "https://core.test.pitstopbetting.com/api/v1/series"
body = ""

def generate_key():
    key = RSA.generate(bits=2048)
    public_key = key.publickey().exportKey("PEM")
    private_key = key.exportKey("PEM")
    with open ('./private_key', 'wb') as pr_file:
        pr_file.write(private_key)
    with open('./public_key', 'wb') as pub_file:
        pub_file.write(public_key)
    enc_public_key = base64.b64encode(public_key)
    print(enc_public_key)
    return key

def get_x_sign():
    with open('./private_key', 'rb') as file:
        key_read = file.read()
    key = RSA.import_key(key_read)
    message = url + body
    msg = message.encode('utf-8')

    h = SHA.new()
    h.update(msg)

    signer = PKCS1_PSS.new(rsa_key=key, saltLen=16)
    signature = signer.sign(msg_hash=h)
    enc_sign = base64.b64encode(signature)
    return str(enc_sign)


#generate_key()
print(get_x_sign())
