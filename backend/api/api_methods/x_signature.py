import base64
import json

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_PSS


class Signature:

    def get_x_sign(self, request_url, request_body, private_key):
        key = RSA.import_key(private_key)
        message = request_url + json.dumps(request_body)
        h = SHA.new()
        h.update(message.encode("utf-8"))
        signer = PKCS1_PSS.new(rsa_key=key, saltLen=16)
        signature = signer.sign(msg_hash=h)
        base64_signature = base64.b64encode(signature)
        return base64_signature
