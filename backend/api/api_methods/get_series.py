import requests

from backend.api.api_methods.app_registration import AppRegistration
from backend.api.api_methods.generation_rsa import GenerationRsaKeys
from backend.api.api_methods.x_signature import Signature


class SeriesList:
    url = "https://core.users_registration.py.pitstopbetting.com/api/v1/series"
    base = GenerationRsaKeys()
    app_reg = AppRegistration(base.public_key)
    x_sign = Signature()

    headers = {"Accept": "application/json",
               "Content-Type": "application/json",
               "Client-Version": "2.0.2",
               "X-App-Id": app_reg.get_x_app_id(),
               "X-Sign": x_sign.get_x_sign(url=url, body="", private_key=base.private_key)}

    def get_series_list(self):
        response = requests.get(self.url, headers=self.headers)
        print(response.text)


