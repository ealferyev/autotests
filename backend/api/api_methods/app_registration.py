import base64
import json

import requests


class AppRegistration:
    url = "https://core.test.pitstopbetting.com/api/v1/app"
    body = ""
    x_app_id = ""

    def __init__(self, public_key):
        base64_pubkey = str(base64.b64encode(public_key)).lstrip("b'").rstrip("'")

        self.body = {
            "platform": 0,
            "os_version": "11.4",
            "pub_key64": base64_pubkey
        }

    def app_registration(self):
        app_registration_request = requests.post(url=self.url, data=json.dumps(obj=self.body))

        app_registration_response = json.loads(app_registration_request.text)

        return app_registration_response

    def get_x_app_id(self):
        response = self.app_registration()

        x_app_id = response['payload']['app_id']
        self.x_app_id = x_app_id
        print("X-App-Id: " + x_app_id)
        #print("Response: " + json.dumps(response.json(), indent=4))
