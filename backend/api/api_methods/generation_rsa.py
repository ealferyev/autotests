from Crypto.PublicKey import RSA


class GenerationRsaKeys:
    public_key = bytes
    private_key = bytes

    def __init__(self):
        """Generate of the RSA key"""
        key = RSA.generate(bits=2048)
        public_key = key.publickey().exportKey("PEM")
        private_key = key.exportKey("PEM")
        self.public_key = public_key
        self.private_key = private_key
