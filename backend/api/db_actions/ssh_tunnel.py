from sshtunnel import SSHTunnelForwarder


class SshTunnel:
    ssh_host = '52.18.247.183'
    ssh_user = 'ealferyev'
    ssh_port = 22
    server = None

    def __init__(self):
        print('Connecting to ' + self.ssh_host + ' via SSH...')
        self.server = SSHTunnelForwarder(
            (self.ssh_host, self.ssh_port),
            ssh_username=self.ssh_user,
            ssh_private_key="/Users/Evgeniy/.ssh/id_rsa",
            remote_bind_address=('172.16.1.59', 5432),
            local_bind_address=('localhost', 5432))

    def start_server(self):
        try:
            self.server.start()
            print("Connection is successful")
        except Exception as e:
            print(e)

    def close_server(self):
        try:
            self.server.close()
        except Exception as e:
            print(e)
