import psycopg2

from backend.api.db_actions.db_config import config
from backend.api.db_actions.ssh_tunnel import SshTunnel


class DataBaseConnector:

    def __init__(self):
        ssh_tunnel = SshTunnel()
        ssh_tunnel.start_server()

    def connection_to_db(self):
        try:
            # read connection parameters
            params = config()
            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            db_connector = psycopg2.connect(**params)
            print('Database session created')
            return db_connector
        except Exception as e:
            print(e)

