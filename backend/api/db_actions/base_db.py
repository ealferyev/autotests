from backend.api.db_actions.database_connector import DataBaseConnector


class BaseDB:
    conn = None

    def __init__(self):
        connect = DataBaseConnector()
        self.conn = connect.connection_to_db()
