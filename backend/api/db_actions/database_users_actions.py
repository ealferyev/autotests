from backend.api.db_actions.base_db import BaseDB


class DataBaseUsersActions(BaseDB):

    # def close_server_connect(self, method):
    #     def wrapped():
    #         result = method()
    #         self.server.close()
    #         return result
    #
    #     return wrapped
    #
    # def create_cursor(self, method):
    #
    #     def wrapped():
    #         cur = self.conn.cursor()
    #         result = method()
    #         return result
    #     return wrapped

    def show_users_list(self):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM users")
        rows = cur.fetchall()
        for row in rows:
            print(row)

    def view_user_by_email(self):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM users WHERE email like '%@pitstopbetting.com'")
        rows = cur.fetchall()
        for row in rows:
            print(row)

    def delete_created_user(self, user_id):
            cur = self.conn.cursor()
            cur.execute("DELETE FROM users WHERE id = {0}".format(user_id))
            self.conn.commit()
            print("User(id = {0}) has been deleted".format(user_id))




#db_users_action = DataBaseUsersActions()
#db_users_action.delete_created_user(105)
