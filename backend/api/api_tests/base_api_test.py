import unittest

from backend.api.api_methods.app_registration import AppRegistration
from backend.api.api_methods.generation_rsa import GenerationRsaKeys
from backend.api.api_methods.x_signature import Signature


class BaseApiTests(unittest.TestCase):
    """Basis for all api_tests."""
    base_url = "https://core.test.pitstopbetting.com/api/v1"
    keygen = GenerationRsaKeys()
    public_key = keygen.public_key
    private_key = keygen.private_key
    app_reg = AppRegistration(public_key=public_key)
    sign = Signature()

