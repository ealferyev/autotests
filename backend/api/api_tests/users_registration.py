from backend.api.api_methods.users_api_methods import UsersApiMethods
from backend.api.api_tests.base_api_test import BaseApiTests


url = "https://core.test.pitstopbetting.com/api/v1/user/register"
body = {
    "first_name": "mister",
    "last_name": "robot",
    "date_of_birth": "1970-01-01",
    "postcode": "101010",
    "email": "qa+mr.robot@pitstopbetting.com",
    "password": "Psb2018"
}


class TestUserRegistration(BaseApiTests):
    user_reg = UsersApiMethods()

    # db_methods = DataBaseUsersActions()

    def user_registr(self):
        self.app_reg.get_x_app_id()
        x_sign = self.sign.get_x_sign(request_url=url, request_body=body, private_key=self.private_key)
        self.user_reg.user_registration(x_sign=x_sign, x_app_id=self.app_reg.x_app_id, url=url, body=body)


user_reg = TestUserRegistration()
user_reg.user_registr()
