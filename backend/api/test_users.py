import json
from backend.api.base_api import BaseApi
import requests


class UsersMethods:

    def user_registration(self, x_sign: str, x_app_id: str, url: str, body: dict):
        user_reg_request = requests.post(url=url,
                                         headers=headers,
                                         data=json.dumps(obj=body)
                                         )
        user_reg_response = json.dumps(user_reg_request.json(), indent=4)
        print(user_reg_response)


    def get_users_info(self, user_id, x_sign: str, x_app_id: str, url: str):
        headers = {"Accept": "application/json",
                   "Content-Type": "application/json",
                   "Client-Version": "ios-2.0.2",
                   "X-App-Id": x_app_id,
                   "X-Sign": x_sign
                   }
        user_info_request = requests.get(url=url+user_id, headers=headers)
        user_info_response = json.dumps(user_info_request.json(), indent=4)
        print(user_info_response)

    def update_user_info(self):
        pass

    def users_balance_info(self):
        pass

    def users_balance_stats(self):
        pass

    def change_active_balance(self):
        pass

    def add_user_to_favs(self):
        pass

    def del_user_from_favs(self):
        pass

    def download_users_avatar(self):
        pass

    def delete_users_avatar(self):
        pass

    def users_logout(self):
        pass