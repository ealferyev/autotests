from appium.webdriver.common.touch_action import TouchAction
from helpers_methods.base_helpers import BaseHelpers

class TapsHelpers(BaseHelpers):

    def tap_spannable_element(self, locator, offset_number):
        element = self.driver.find_element_by_id(locator)

        """getting element location coordinates"""
        point = element.location

        """calculating offset to the hyperlink text"""
        offset_size = element.size['width'] * offset_number
        x_offset = point['x'] + offset_size
        y_offset = point['y'] + (element.size['height'] / 2)

        """performing tap on the hyperlink text"""
        action = TouchAction(self.driver)
        action.tap(element=None, x=x_offset, y=y_offset).perform()