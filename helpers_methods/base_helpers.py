
class BaseHelpers:
    def __init__(self, driver):
        """ Base constructor.

            Sets driver, implicit wait, and timeout.
        """
        self.driver = driver