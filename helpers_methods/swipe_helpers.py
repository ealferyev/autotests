from appium.webdriver.common.touch_action import TouchAction
from selenium.common.exceptions import NoSuchElementException

from helpers_methods.base_helpers import BaseHelpers


class SwipeHelpers(BaseHelpers):

    def horizontal_scroll_and_click_element(self, scrollable_element, found_element):
        scroll_element = self.driver.find_element_by_android_uiautomator(
            'new UiScrollable(new UiSelector().resourceId("{scrollable_element}"))'
            '.setAsHorizontalList().scrollIntoView(new UiSelector().text("{found_element}"))'
            .format(scrollable_element=scrollable_element, found_element=found_element))
        scroll_element.click()

    def vertical_scroll_and_click_element(self, scrollable_element, found_element):
        scroll_element = self.driver.find_element_by_android_uiautomator(
            'new UiScrollable(new UiSelector().resourceId("{scrollable_element}"))'
            '.setAsVerticalList().scrollIntoView(new UiSelector().text("{found_element}"))'
            .format(scrollable_element=scrollable_element, found_element=found_element))
        scroll_element.click()



    def swipe_action(self, swipe_type, swipe_direction, scrollable_element):
        """
            Swipe type: 'vertical', 'horizontal'
            Swipe direction: 'up', 'down', 'left', 'right'
            Scrollable element: the element, which necessary to scroll
        """
        element_size = self.driver.find_element_by_id(scrollable_element).size
        print(element_size)
        action = TouchAction(self.driver)
        if swipe_type == "vertical":
            start_y = float(element_size['height'] * 0.5)
            end_y = float(element_size['height'] * 0.2)
            start_x = float(element_size['width'] / 2)
            if swipe_direction == "up":
                action.press(x=start_x, y=end_y).move_to(x=start_x, y=start_y).release().perform()
            elif swipe_direction == "down":
                action.press(x=start_x, y=start_y).move_to(x=start_x, y=end_y).release().perform()
        elif swipe_type == "horizontal":
            start_x = float(element_size['width'] * 0.7)
            end_x = float(element_size['width'] * 0.3)
            start_y = float(element_size['height'] / 2)
            if swipe_direction == "left":
                action.press(x=end_x, y=start_y).move_to(x=start_x, y=start_y).release().perform()
            elif swipe_direction == "right":
                action.press(x=start_x, y=start_y).wait(500).move_to(x=end_x, y=start_y).release().perform()

    def swipe_to_element(self, swipe_type, swipe_direction, locator_type, element, scrollable_element):
        found = False
        while found == False:
            search_element = None
            try:
                if locator_type == "id":
                    search_element = self.driver.find_element_by_id(element)
                elif locator_type == "xpath":
                    search_element = self.driver.find_element_by_xpath(element)
                found = True
                search_element.click()
            except NoSuchElementException:
                self.swipe_action(swipe_type=swipe_type, swipe_direction=swipe_direction,
                                  scrollable_element=scrollable_element)

    def scroll_action_to_element(self, element, locator, click, start_point, end_point):
        """ Выполнение скролинга с целью поиска эелемента который невидим на экране.
                element - в этот аргумент передаем id или xpath локатор элемента, который надо найти
                click - в этот аргумент передается значение True или False, в зависимости от того,
                        нужно ли выполнить нажатие на элемент
                success - показывает, найден ли элемент на экране в процессе цикла,
                        если нет - выполняется новая итерация, success получает пустое значение
                locator - в этот аргумент передается тип локатора: id, xpath, и тд
        """
        global item
        success = False
        while success is False:
            try:
                success = ""
                self.element = element
                if locator == "xpath":
                    item = self.driver.find_element_by_xpath(element)
                elif locator == "id":
                    item = self.driver.find_element_by_id(element)
                else:
                    print("Locator not valid")

                if click is True:
                    item.click()
            except NoSuchElementException:
                dimension = self.driver.manage().window().size
                print(dimension)
                #                isEnd = "end"
                #                if isEnd is True:
                #                    return "notfound"
                touch = TouchAction(self.driver)
                touch.press(start_point).move_to(end_point).release().perform()
                success = False
        return "found"
