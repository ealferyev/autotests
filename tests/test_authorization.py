from pages.login_page import LoginPage
from pages.main_page import MainPage
from pages.my_book_page import MyBookPage
from pages.pin_code_page import PinCodePage
from pages.settings_page import SettingsPage
from pages.alerts import ResendEmailAlert
from tests.base_test import BaseTest


class AuthorizationTest(BaseTest):

    USER_PHONE = '+79089234848'
    VALID_EMAIL = 'ealferyev+fb@splinex.com'
    INVALID_EMAIL = 'qwerty123@splinex.com'
    UNCONFIRM_EMAIL = 'ealferyev+notconfirm@splinex.com'
    VALID_PASSWORD = '123456789'
    INVALID_PASSWORD = '12345678'

    def setUp(self):
        """Sets up Appium connection and navigates to login page."""
        BaseTest.setUp(self)
        self.login_page = LoginPage(self.driver)
        self.pin_code_page = PinCodePage(self.driver)
        self.main_page = MainPage(self.driver)
        self.my_book_page = MyBookPage(self.driver)
        self.settings_page = SettingsPage(self.driver)
        self.resend_alert = ResendEmailAlert(self.driver)


    def test_valid_login_via_email(self):

        """Login with valid credentials, enter pincode, log out."""
        self.login_page.log_in(email=self.VALID_EMAIL, password=self.VALID_PASSWORD)
        self.assertTrue(self.pin_code_page.title_is_displays())
        self.pin_code_page.enter_pin_code('num2', 'num4', 'num0', 'num7')
        self.pin_code_page.enter_confirm_pin_code('num2', 'num4', 'num0', 'num7')
        self.main_page.open_my_book()
        self.my_book_page.open_settings()
        self.settings_page.perform_users_logout()



    def test_login_by_incorrect_password(self):
        """Login with incorrect password"""
        self.login_page.log_in(email=self.VALID_EMAIL, password=self.INVALID_PASSWORD)
        self.assertTrue(self.login_page.is_invalid_login_message_displayed())

    def test_login_by_incorrect_email(self):
        """Login with incorrect email"""
        self.login_page.log_in(email=self.INVALID_EMAIL, password=self.VALID_PASSWORD)
        self.assertTrue(self.login_page.is_invalid_login_message_displayed())

    def test_login_with_unconfirm_email(self):
        """Login with unconfirmed email"""
        self.login_page.log_in(email=self.UNCONFIRM_EMAIL, password=self.VALID_PASSWORD)
        self.assertTrue(self.resend_alert.resend_msg_is_displays())

    def test_login_with_unconfirm_phone(self):
        """Login with unconfirmed phone"""
        pass

    #def test_login_by_pincode(self):
    #    self.pin_code_page.enter_pin_code()
    #    self.main_page.open_my_book()
    #    self.my_book_page.open_settings()

    def test_example(self):
        self.login_page.click_join_the_game()
