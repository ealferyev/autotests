from tests.base_test import BaseTest
from pages.onboarding_page import OnboardingPage

class OnboardingTest(BaseTest):
    def setUp(self):
        BaseTest.setUp(self)
        self.onboarding_page = OnboardingPage(self.driver)

    def test_open_sing_up_form(self):
        self.onboarding_page.click_sign_up_button()