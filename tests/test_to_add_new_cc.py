from pages.balance_page import BalancePage
from pages.billing_address_page import BillingAddressPage
from pages.card_details_page import CardDetailsPage
from pages.deposit_page import DepositPage
from pages.main_page import MainPage
from pages.my_book_page import MyBookPage
from pages.pin_code_page import PinCodePage
from tests.base_test import BaseTest


class AddNewCardTest(BaseTest):
    card_number_mc = "4200000000000000"
    expiry_date = "1221"
    cvv_code = "123"
    card_holder_name = "John Doe"

    city = "Manchester"
    address = "1, 3 Union St"
    zip_code = "M1 1AG"

    def setUp(self):
        BaseTest.setUp(self)
        self.pin_code_page = PinCodePage(self.driver)
        self.main_page = MainPage(self.driver)
        self.my_book_page = MyBookPage(self.driver)
        self.balance_page = BalancePage(self.driver)
        self.deposit_page = DepositPage(self.driver)
        self.card_details_page = CardDetailsPage(self.driver)
        self.billing_address_page = BillingAddressPage(self.driver)

    def test_add_new_card(self):
        self.pin_code_page.enter_pin_code('num2', 'num4', 'num0', 'num7')
        self.main_page.open_my_book()
        self.my_book_page.open_balance_screen()
        self.balance_page.click_deposit_button()
        self.deposit_page.click_to_add_new_card()
        self.card_details_page.type_users_card_details(card_number=self.card_number_mc,
                                                       expiry_date=self.expiry_date,
                                                       cvv_code=self.cvv_code,
                                                       card_holder_name=self.card_holder_name)
        self.billing_address_page.type_billing_address(city=self.city,
                                                       address=self.address,
                                                       zip_code=self.zip_code)
