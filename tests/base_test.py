import unittest

from appium import webdriver


class BaseTest(unittest.TestCase):
    """Basis for all tests."""

    def setUp(self):
        """Sets up desired capabilities and the Appium driver."""

        url = 'http://127.0.0.1:4723/wd/hub'
        desired_caps = {}
        desired_caps['platformName'] = 'android'
        desired_caps['platformVersion'] = '8.1.0'
        desired_caps['deviceName'] = 'Nexus 5X'
        #desired_caps['platformVersion'] = '5.1.1'
        #desired_caps['deviceName'] = 'Lenovo A6020'
        desired_caps['noReset'] = 'true'
        desired_caps['appPackage'] = 'com.motorsport.psb.test.debug'
        desired_caps['appActivity'] = 'com.motorsport.psb.presentation.launch.view.LaunchActivity'
        desired_caps["automationName"] = 'uiautomator2'
        desired_caps["unicodeKeyboard"] = 'true'
        desired_caps["resetKeyboard"] = 'true'

        self.driver = webdriver.Remote(url, desired_caps)
        self.driver.implicitly_wait(30)



#    def tearDown(self):
#      self.driver.quit()
